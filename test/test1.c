/*
 * Copyright (c) 2015-2017 Devin Smith <devin@devinsmith.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>

#include <xui.h>

static int
msg_event(xuiWnd *w, int msg, int iparam, void *pparam)
{
  return xui_window_default_proc(w, msg, iparam, pparam);
}

int
main(int argc, char *argv[])
{
  xuiWnd *top;
  xuiWnd *button;
  struct xuiClass win_class;

  /* Open display */
  xui_init(NULL);

  win_class.name = "ExampleApp";
  win_class.eventProc = msg_event;
  win_class.wndExtra = 0;
  win_class.background = XUIRGB(212, 208, 200);

  xui_class_register(&win_class);

  top = xui_window_create("ExampleApp", "Test Program", 0,
      200, 200, 500, 300, NULL);
  button = xui_window_create("Button", "Press me", 0, 50, 50, 100, 50, top);
  xui_window_show(top);

  xui_loop();

  xui_shutdown();
  return 0;
}
