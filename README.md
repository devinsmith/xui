# xui
A graphical user interface toolkit for X written in C99.

# Requirements
Debian based systems require the following libraries:
```
sudo apt-get install libx11-dev libxft-dev libxpm-dev
```

VoidLinux requires the following libraries:
```
sudo xbps-install libX11-devel libXft-devel libXpm-devel
```

Fedora requires the following libraries:
```
sudo dnf install libX11-devel libXft-devel libXpm-devel xorg-x11-fonts-misc
```

# Building
You can use cmake to build the static library and tests.
```
mkdir build
cd build
cmake ..
make
```
This will create a libxui.a library and build a sample app
that utilizes the library.

Alternatively, Makefiles are provided but may need to be edited for your
platform. Simply change to the src directory and execute make.
This will build a static library called libxui.a.
You can then change into the test directory to build a sample
app that utilizes the library.


