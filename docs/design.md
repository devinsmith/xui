
# XUI Graphical Toolkit
A graphical user interface toolkit for X written in C99.

# Ideas
Based on lots of other toolkits and libraries (Win32, GTK+, FLTK, FOX toolkit, etc).

Like GTK+ all widgets are a single type (struct xuiWnd) that is opaque to the end user.
Going forward we call these "windows".

A window is an abstraction over a Xlib Window and defined by struct xuiWnd:
```cpp
struct xuiWnd {
  Window window; /* XLib window */
  int style;
  struct xuiWnd *parent; /* Parent window */
  void *userExtra; /* user supplied extra stuff ('this' pointer for C++) */

  int x, y, width, height;
  int (*eventProc)(struct xuiWnd *wnd, unsigned int msg, void *msgParam, void *extra);

  struct xuiLM *layout_manager;
  /* Scroll bars */
  struct xuiSB *horiz_sb;
  struct xuiSB *vert_sb;

  /* Menubar */
  struct xuiMenu *menu;

  char wndExtra[]; /* control specific properties */
};
```

XUI Windows can be created with the following API:

```cpp
struct xuiWnd *xui_window_create(const char *className, int classOptions,
    const char *windowName, int x, int y, int width, int height,
    struct xuiWnd *parent);
```

Where className is the name of a class that defines the window/widget type.
classOptions are options that are applicable to the given class.

```cpp
typedef struct xuiWndClass {
  const char *className;
  int rgbBackground; /* RGB triplet */
  int (*eventProc)(struct xuiWnd *wnd, unsigned int msg, void *msgParam, void *extra);
  size_t wndExtra;
} xuiWndClass;

bool xuiRegisterWndClass(struct xuiWndClass *wndClass);
```

# Event types

A window event procedure (eventProc) might receive the following messages in the msg parameter

```cpp
enum event_types {
  MSG_PAINT,
  MSG_CLICK,
  MSG_FOCUSIN,
  MSG_FOCUSOUT,
  /* ... more */
  MAX_EVENT_TYPE
}
```

