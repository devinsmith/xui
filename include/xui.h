/*
 * Copyright (c) 2008-2018 Devin Smith <devin@devinsmith.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef XUI_H
#define XUI_H

#include <stdbool.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <xuidefs.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Defines */

/* Window styles */
#define WS_TABSTOP 0x00010000L

/* Window messages, mostly based on Win32 */
#define WM_CREATE                       0x0001
#define WM_DESTROY                      0x0002
#define WM_SIZE                         0x0005
#define WM_SETTEXT                      0x000C
#define WM_GETTEXT                      0x000D
#define WM_PAINT                        0x000F
#define WM_CLOSE                        0x0010
#define WM_LBUTTONDOWN                  0x0201
#define WM_LBUTTONUP                    0x0202

/* Need to be re-written */
#define MSG_MOUSEENTER 0xEE000
#define MSG_MOUSELEAVE 0xEE01
#define MSG_MOUSEBUTTONUP 0xEE02
#define MSG_MOUSEBUTTONDOWN 0xEE03
#define MSG_COMMAND 0XEE04
#define MSG_KEYPRESS 0xEE05


#define KEY_SHIFTMASK 0x001 /* Shift key is down */

/* Stock Colors */
enum xui_stock_colors {
  COLOR_WHITE = 0,
  COLOR_BLACK,
  COLOR_FRAMEBACK,
  COLOR_FRAMEHILITE,
  COLOR_FRAMESHADOW,
  COLOR_MAX
};

/* Stock fonts */
enum xui_stock_fonts {
  FONT_NORMAL = 0,
  FONT_MAX
};

/* Forward declarations */
struct xuiDC;
struct xuiWnd;
struct xuiFont;

typedef struct xuiWnd xuiWnd;
typedef struct xui_create_struct {
  int cx;
  int cy;
  int x;
  int y;
  unsigned int style;
  const char *name;
  const char *classname;
} xui_create_struct;

struct xuiClass {
  const char *name;
  int (*eventProc)(xuiWnd *wnd, int msg, int iparam, void *pparam);
  unsigned int background;
  size_t wndExtra;
};

struct xui_event {
  int keycode;
  int state;

  int x;
  int y;
  char keytext[64];
};


struct xuiRect {
  int x;
  int y;
  int width;
  int height;
};

/* Prototypes */
void xui_init(const char *display_name);
void xui_loop(void);
void xui_stop(void);
void xui_shutdown(void);

int xui_class_register(struct xuiClass *wndClass);

xuiWnd *xui_window_create(const char *lpClassName,
  const char *lpWindowName, unsigned int style, int x, int y, int width,
  int height, struct xuiWnd *parent);
void xui_window_show(struct xuiWnd *w);
void xui_window_map_subwindows(struct xuiWnd *w);
int xui_window_send(struct xuiWnd *w, int msgId, int arg1, void *arg3);
int xui_window_default_proc(xuiWnd *w, int msgId, int argi, void *argp);
void xui_window_set_name(struct xuiWnd *wnd, const char *name);
void xui_window_destroy(struct xuiWnd *wnd);

const char *xui_window_get_text(struct xuiWnd *wnd);
void *xui_window_get_extra(struct xuiWnd *wnd);
void xui_window_get_rect(struct xuiWnd *wnd, struct xuiRect *r);
xuiWnd *xui_window_get_sibling(xuiWnd *parent, xuiWnd *starting, int reverse);
void xui_window_invalidate(xuiWnd *wnd);

void xui_res_init(void);
struct xuiFont *xui_res_create_font(const char *name);
const struct xuiFont *xui_res_get_stock_font(int font_type);
unsigned int xui_res_get_rgb_color(unsigned int rgbColor);

struct xuiDC *xui_dc_start(struct xuiWnd *wnd);
void xui_dc_end(struct xuiDC *dc);
void xui_dc_draw_rectangle(struct xuiDC *dc, int x, int y, int w, int h);
void xui_dc_draw_line(struct xuiDC *dc, int x1, int y1, int x2, int y2);
void xui_dc_set_font(struct xuiDC *dc, const struct xuiFont *font);
void xui_dc_set_foreground(struct xuiDC *dc, unsigned int clr_indx);
void xui_dc_draw_text(struct xuiDC *dc, int x, int y, const char *text,
    size_t tlen);
void xui_dc_fill_rectangle(struct xuiDC *dc, int x, int y, int w, int h);

#ifdef __cplusplus
}
#endif

#endif /* XUI_H */
