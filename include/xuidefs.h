/*
 * Copyright (c) 2017 Devin Smith <devin@devinsmith.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef __XUIDEFS_H__
#define __XUIDEFS_H__

/* Return the maximum of a or b */
#define XUIMAX(a,b) (((a)>(b))?(a):(b))

/* Return the minimum of a or b */
#define XUIMIN(a,b) (((a)>(b))?(b):(a))

/* Get red value (0-ff) from a RGBA color quad */
#define XUIREDVAL(rgba)     ((unsigned char)(((rgba)>>24)&0xff))

/* Get green value (0-ff) from a RGBA color qud */
#define XUIGREENVAL(rgba)   ((unsigned char)(((rgba)>>16)&0xff))

/* Get blue value (0-ff) from a RGBA color quad */
#define XUIBLUEVAL(rgba)    ((unsigned char)(((rgba)>>8)&0xff))

/* Get alpha value (0-ff) from a RGBA color quad */
#define XUIALPHAVAL(rgba)   ((unsigned char)((rgba)&0xff))

/* Returns an unsigned integer representing a RGB color triplet. */
#define XUIRGB(r,g,b)   (((unsigned int)(unsigned char)(r)<<24) | ((unsigned int)(unsigned char)(g)<<16) | ((unsigned int)(unsigned char)(b)<<8) | 0x000000ff)

#endif /* __XUIDEFS_H__ */
