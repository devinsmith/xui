/*
 * Copyright (c) 2017 Devin Smith <devin@devinsmith.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <xui.h>
#include "xui_priv.h"

struct xuiDC {
  struct xuiWnd *wnd;
  const struct xuiFont *font;
  GC gc;
};

struct xuiDC *
xui_dc_create(struct xuiWnd *wnd)
{
  XGCValues gcv;
  struct xuiDC *dc;

  dc = malloc(sizeof(struct xuiDC));

  dc->font = NULL;
  dc->wnd = wnd;

  gcv.background = WhitePixel(xuiAppCtx.display, DefaultScreen(xuiAppCtx.display));
  gcv.foreground = BlackPixel(xuiAppCtx.display, DefaultScreen(xuiAppCtx.display));
  dc->gc = XCreateGC(xuiAppCtx.display, DefaultRootWindow(xuiAppCtx.display),
      GCForeground | GCBackground, &gcv);

  return dc;
}

struct xuiDC *
xui_dc_start(struct xuiWnd *wnd)
{
  wnd->is_dirty = 0;
  return wnd->dc;
}

void
xui_dc_draw_rectangle(struct xuiDC *dc, int x, int y, int w, int h)
{
  XDrawRectangle(xuiAppCtx.display, dc->wnd->win, dc->gc, x, y, w, h);
}

void
xui_dc_fill_rectangle(struct xuiDC *dc, int x, int y, int w, int h)
{
  XFillRectangle(xuiAppCtx.display, dc->wnd->win, dc->gc, x, y, w, h);
}

void
xui_dc_draw_line(struct xuiDC *dc, int x1, int y1, int x2, int y2)
{
  XDrawLine(xuiAppCtx.display, dc->wnd->win, dc->gc, x1, y1, x2, y2);
}

void
xui_dc_set_font(struct xuiDC *dc, const struct xuiFont *font)
{
  dc->font = font;
}

void
xui_dc_set_foreground(struct xuiDC *dc, unsigned int clr_indx)
{
  Pixel pix = xui_res_get_color(clr_indx);
  XSetForeground(xuiAppCtx.display, dc->gc, pix); 
}

void
xui_dc_draw_text(struct xuiDC *dc, int x, int y, const char *string,
    size_t string_len)
{
  XFontStruct *font;
  XTextItem ti[1];

  if (dc->font == NULL)
    return;

  font = dc->font->id;

  ti[0].chars = (char *)string;
  ti[0].nchars = string_len;
  ti[0].delta = 0;
  ti[0].font = font->fid;

  XDrawText(xuiAppCtx.display, dc->wnd->win, dc->gc, x,
      (y - (font->ascent + font->descent)) / 2 + font->ascent, ti, 1);

  //XUnloadFont(disp, font->fid);
}

void
xui_dc_end(struct xuiDC *dc)
{
}

