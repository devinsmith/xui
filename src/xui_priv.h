/*
 * Copyright (c) 2008-2018 Devin Smith <devin@devinsmith.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* Defines numerous private structures, functions, and variables
 * for use by XUI internals */

#ifndef __XUI_PRIV_H__
#define __XUI_PRIV_H__

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xresource.h>
#include <X11/xpm.h>

struct xuiWnd {
  xuiWnd *parent;
  Window win; /* X11 window object. */
  unsigned int style;
  int x;
  int y;
  int width;
  int height;
  char *text;
  bool is_dirty;
  unsigned long background_pixel;

  /* A window can have it's own gc or share a system gc */
  struct xuiDC *dc;

  int (*handleMessage)(struct xuiWnd *wnd, int msgId, int arg1, void *arg2);

  /* windows can have children, who can have children, who can .... */
  size_t n_children;
  xuiWnd **children;


  /* This must be the last member of the struct. */
  char wndExtra[];
};

struct xuiFont {
  XFontStruct *id; /* font */
};

struct xuiContext {
  Display *display;
  GC gc;
  Colormap colormap;

  /* Last clicked window */
  xuiWnd *mouse_window;

  xuiWnd *selection_window;

  /* Last event */
  struct xui_event last_event;
};


extern struct xuiContext xuiAppCtx;

struct xuiDC *xui_dc_create(struct xuiWnd *w);

struct xuiClassP {
  char name[256];
  int (*eventProc)(xuiWnd *wnd, int msg, int iparam, void *pparam);
  unsigned long background_pixel;
  size_t wndExtra;
};

struct xuiClassP *xui_class_get(const char *classname);
Pixel xui_res_get_color(int colorIndex);

#endif /* __XUI_PRIV_H__ */
