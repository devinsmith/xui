/*
 * Copyright (c) 2015-2017 Devin Smith <devin@devinsmith.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include <xui.h>

#define RBDIAM 11
#define RB_X 8

struct buttonInfo {
  int activated;
};

static int handleMessage(struct xuiWnd *w, int msgId, int arg1, void *arg3);

struct xuiClass ButtonClass = {
  "Button", handleMessage, XUIRGB(0xff, 0xff, 0xff), sizeof(struct buttonInfo)
};

static void
drawRadioButton(struct xuiWnd *wnd, int clicked)
{
  struct buttonInfo *extra;
  const char *label;
  const struct xuiFont *font;
  struct xuiDC *dc;
  struct xuiRect r;

  font = xui_res_get_stock_font(FONT_NORMAL);
  label = xui_window_get_text(wnd);
  extra = xui_window_get_extra(wnd);
  xui_window_get_rect(wnd, &r);

  dc = xui_dc_start(wnd);

  /* draw double raised rectangle */
  xui_dc_set_foreground(dc, COLOR_BLACK);
  xui_dc_fill_rectangle(dc, 0, r.height - 1, r.width, 1);
  xui_dc_fill_rectangle(dc, r.width - 1, 0, 1, r.height);
  xui_dc_set_foreground(dc, COLOR_FRAMEHILITE);
  xui_dc_fill_rectangle(dc, 0, 0, r.width - 1, 1);
  xui_dc_fill_rectangle(dc, 0, 0, 1, r.height - 1);

  xui_dc_set_foreground(dc, COLOR_FRAMEBACK);
  xui_dc_fill_rectangle(dc, 1, 1, r.width - 2, 1);
  xui_dc_fill_rectangle(dc, 1, 1, 1, r.height - 2);
  xui_dc_set_foreground(dc, COLOR_FRAMESHADOW);
  xui_dc_fill_rectangle(dc, 1, r.height - 2, r.width - 2, 1);
  xui_dc_fill_rectangle(dc, r.width - 2, 1, 1, r.height - 2);

  xui_dc_set_foreground(dc, COLOR_BLACK);
  xui_dc_set_font(dc, font);
  xui_dc_draw_text(dc, RB_X * 2 + RBDIAM, r.height, label, strlen(label));
  xui_dc_end(dc);
}

static int
handleMessage(struct xuiWnd *w, int msgId, int arg1, void *arg2)
{
  switch (msgId) {
  case WM_LBUTTONDOWN:
    drawRadioButton(w, 1);
    break;
  case WM_LBUTTONUP:
    drawRadioButton(w, 0);
    break;
  case WM_PAINT:
    drawRadioButton(w, 0);
    return 0;
    break;
  default:
    return xui_window_default_proc(w, msgId, arg1, arg2);
    break;
  }

  return 1;
}
