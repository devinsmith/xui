/*
 * Copyright (c) 2008-2018 Devin Smith <devin@devinsmith.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h> /* for XContext functions */
#include <X11/Xresource.h>

#include <xui.h>
#include <xui_priv.h>

extern Atom WM_DELETE_WINDOW;
extern XContext xuiG_window_ctxt;

static void
add_child(xuiWnd *parent, xuiWnd *child)
{
  xuiWnd **children;

  children = realloc(parent->children, sizeof(xuiWnd *) *
      (parent->n_children + 1));
  children[parent->n_children] = child;
  parent->n_children++;
  parent->children = children;
}

xuiWnd *
xui_window_create(const char *classname, const char *title, unsigned int style,
    int x, int y, int width, int height, xuiWnd *parent)
{
  xuiWnd *ret;
  XClassHint class_hint;
  struct xuiClassP *wc;
  unsigned long mask;
  XSetWindowAttributes attr;
  xui_create_struct cs;
  int screen_num;

  wc = xui_class_get(classname);
  if (wc == NULL) {
    /* Can't create window, no class */
    fprintf(stderr, "Can't create window, no %s class!\n", classname);
    return NULL;
  }

  /* XXX: This does set wc->wndExtra all to 0, but we should really handle
   * that in each widget's create process. Currently some widgets depend
   * on this zero initialization to occur. */
  ret = calloc(1, sizeof(xuiWnd) + wc->wndExtra);
  if (ret == NULL)
    return ret;

  ret->handleMessage = wc->eventProc;

  ret->x = x;
  ret->y = y;
  ret->width = width;
  ret->height = height;
  ret->style = style;
  ret->parent = parent;
  ret->background_pixel = wc->background_pixel;

  if (title != NULL) {
    /* XXX: Casting away the const here, but so far all receivers are safe */
    xui_window_send(ret, WM_SETTEXT, 0, (void *)title);
  }

  screen_num = DefaultScreen(xuiAppCtx.display);

  if (parent == NULL) {
    /*
     * create a X window, as a direct child of the screen's
     * root window. Use the screen's black and white colors as
     * the foreground and background colors of the window,
     * respectively. Place the new window's top-left corner at
     * the given 'x,y' coordinates.
     */
    mask = CWBackPixel | CWBorderPixel;
    attr.background_pixel = wc->background_pixel;
    attr.border_pixel = BlackPixel(xuiAppCtx.display, screen_num);
    ret->win = XCreateWindow(xuiAppCtx.display, RootWindow(xuiAppCtx.display, screen_num),
      x, y, width, height, 2 /* border width */, CopyFromParent, /* depth */
      CopyFromParent /* class */, CopyFromParent /* visual */, mask,
      &attr);

    /* Use the WM_DELETE_WINDOW atom to tell the window manager that we want
     * to handle when this window is closed/destroyed */
    XSetWMProtocols(xuiAppCtx.display, ret->win, &WM_DELETE_WINDOW, 1);
  } else {
    mask = CWBackPixel;
    attr.background_pixel = WhitePixel(xuiAppCtx.display, screen_num);
    ret->win = XCreateWindow(xuiAppCtx.display, parent->win,
      x, y, width, height, 0, CopyFromParent, /* depth */
      CopyFromParent /* class */, CopyFromParent /* visual */, mask, &attr);
    xui_window_show(ret);

    add_child(parent, ret);
  }

  ret->dc = xui_dc_create(ret);

  cs.cx = width;
  cs.cy = height;
  cs.x = x;
  cs.y = y;
  cs.style = style;
  cs.classname = classname;
  cs.name = title;
  xui_window_send(ret, WM_CREATE, 0, &cs);

  class_hint.res_name = ret->text;
  class_hint.res_class = (char *)wc->name;
  XSetClassHint(xuiAppCtx.display, ret->win, &class_hint);

  /* Setup up an input event queue */
  XSelectInput(xuiAppCtx.display, ret->win, ExposureMask |
      ButtonPressMask | ButtonReleaseMask | KeyPressMask |
      EnterWindowMask | LeaveWindowMask);

  XSaveContext(xuiAppCtx.display, ret->win, xuiG_window_ctxt, (XPointer)ret);

  /* For top level windows we want to do some extra special case
   * processing */
  if (parent == NULL) {
    xui_window_set_name(ret, title);
  }

  return ret;
}

void
xui_window_set_name(xuiWnd *wnd, const char *name)
{
  XTextProperty wname;

  if (XStringListToTextProperty((char **)&name, 1, &wname) == 0) {
    fprintf(stderr, "Can't allocate window name\n");
    return;
  }
  XSetWMIconName(xuiAppCtx.display, wnd->win, &wname);
  XSetWMName(xuiAppCtx.display, wnd->win, &wname);
  XFree(wname.value);
}

void
xui_window_show(xuiWnd *w)
{
  XMapWindow(xuiAppCtx.display, w->win);
}

void
xui_window_close(xuiWnd *w)
{
  xui_window_destroy(w);
  XDestroyWindow(xuiAppCtx.display, w->win);
  free(w);
}

int
xui_window_send(xuiWnd *w, int msgId, int arg1, void *arg2)
{
  if (w->handleMessage != NULL) {
    return w->handleMessage(w, msgId, arg1, arg2);
  }
  return xui_window_default_proc(w, msgId, arg1, arg2);
}

/* Similar to Win32 GetNextDlgTabItem */
xuiWnd *
xui_window_get_sibling(xuiWnd *parent, xuiWnd *starting, int reverse)
{
  size_t i, j;
  xuiWnd *search = NULL;

  if (parent->n_children == 0)
    return starting;

  /* First find the starting point, by iterating through all children.
   * Depending if we're searching backwards or forwards our starting point
   * will be 1 before or 1 after the starting window. */
  for (i = 0; i < parent->n_children; i++) {
    xuiWnd *n;

    /* Check children */
    n = parent->children[i];
    if (n == starting) {
      if (!reverse) {
        j = i + 1;
        if (j >= parent->n_children) {
          j = 0;
        }
      } else {
        if (i == 0) {
          j = parent->n_children - 1;
        } else {
          j = i - 1;
        }
      }
      search = parent->children[j];
      break;
    }
  }

  /* Starting window was not found in parents children */
  if (search == NULL)
    return starting;


  while (search != starting) {
    if (search->style & WS_TABSTOP)
      return search;

    /* Otherwise advance or reverse */
    if (!reverse) {
      j++;
      if (j >= parent->n_children) {
        j = 0;
      }
    } else {
      if (j == 0) {
        j = parent->n_children - 1;
      } else {
        j--;
      }
    }
    search = parent->children[j];
  }

  return starting;
}

void
xui_window_destroy(xuiWnd *w)
{
  size_t i;
  for (i = 0; i < w->n_children; i++) {
    xuiWnd *n;

    /* Check children */
    n = w->children[i];
    xui_window_destroy(n);

    xui_window_send(n, WM_DESTROY, 0, NULL);
    free(n);
  }
  free(w->children);
}

const char *
xui_window_get_text(xuiWnd *w)
{
  return w->text;
}

void *
xui_window_get_extra(struct xuiWnd *wnd)
{
  return wnd->wndExtra;
}

/* Gets the client area rectangle */
void
xui_window_get_rect(struct xuiWnd *wnd, struct xuiRect *r)
{
  if (r == NULL)
    return;

  r->x = 0;
  r->y = 0;
  r->width = wnd->width;
  r->height = wnd->height;
}

void
xui_window_set_text(xuiWnd *w, char *text)
{
  w->text = text;
}


int
xui_window_default_proc(xuiWnd *w, int msgId, int arg1, void *argp)
{
  const char *title;

  switch (msgId) {
  case WM_CLOSE:
    printf("Request to close window\n");
    xui_window_destroy(w);
    break;
  case WM_DESTROY:
    break;
  case WM_PAINT:
    w->is_dirty = 0;
    break;
  case WM_SETTEXT:
    title = argp;

    if (w->text != NULL) {
      free(w->text);
    }
    w->text = strdup(title);
    break;
  default:
    printf("Unhandled message\n");
    break;
  }
  return 0;
}
