/*
 * Copyright (c) 2015-2017 Devin Smith <devin@devinsmith.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xresource.h>
#include <X11/xpm.h>

#include <xui.h>
#include <xui_priv.h>

struct class_list {
  size_t n;
  size_t capacity;
  struct xuiClassP *classes;
};

static struct class_list class_list;

struct xuiClassP *
xui_class_get(const char *name)
{
  size_t i;
  struct xuiClassP *wc;

  /* Iterate through list of window classes to see if this
   * class has already been registered. */
  for (i = 0; i < class_list.n; i++) {
    wc = &class_list.classes[i];
    if (strcmp(wc->name, name) == 0) {
      return wc;
    }
  }

  return NULL;
}

/* Registers a new xui class */
int
xui_class_register(struct xuiClass *wndClass)
{
  struct xuiClassP *wc;
  XColor back_col;
  char color_template[14]; /* rgb:xx/yy/zz */

  if (wndClass == NULL)
    return -1;

  wc = xui_class_get(wndClass->name);
  if (wc != NULL) {
    /* Already registered */
    return 1;
  }

  /* Resize our array if necessary. */
  if (class_list.n >= class_list.capacity) {
    struct xuiClassP *resized;
    if (class_list.capacity == 0) {
      class_list.capacity = 2;
    } else {
      class_list.capacity = class_list.capacity << 1;
    }
    if ((resized = realloc(class_list.classes,
        sizeof(struct xuiClassP) * class_list.capacity)) == NULL) {
      fprintf(stderr, "Failed to re-allocate class list\n");
      return -1;
    }
    class_list.classes = resized;
  }

  /* Register a new class */
  wc = &class_list.classes[class_list.n];
  strncpy(wc->name, wndClass->name, sizeof(wc->name) - 1);
  wc->name[sizeof(wc->name) - 1] = '\0';
  wc->wndExtra = wndClass->wndExtra;
  wc->eventProc = wndClass->eventProc;

  /* Determine background_pixel */
  snprintf(color_template, sizeof(color_template), "rgb:%02x/%02x/%02x",
      XUIREDVAL(wndClass->background), XUIGREENVAL(wndClass->background),
      XUIBLUEVAL(wndClass->background));

  /* Lookup colors */
  XParseColor(xuiAppCtx.display, xuiAppCtx.colormap,
      color_template, &back_col);
  XAllocColor(xuiAppCtx.display, xuiAppCtx.colormap, &back_col);

  wc->background_pixel = back_col.pixel;

  class_list.n++;
  return 0;
}
