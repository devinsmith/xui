/*
 * Copyright (c) 2008-2018 Devin Smith <devin@devinsmith.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <errno.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h> /* For XContext and others */
#include <X11/Xresource.h>

#include <xui.h>
#include <xuikeys.h>

#include <xui_priv.h>

/* Global variables */
XContext xuiG_window_ctxt;

static xuiWnd *focused_window;

struct xuiContext xuiAppCtx;

struct paint_entry {
  xuiWnd *wnd;

  struct paint_entry *next;
};

static struct paint_entry *paint_queue;

/* Special atom for deleted messages. */
Atom WM_DELETE_WINDOW;


static int x_fd;
static bool loop_done;
static bool any_win_dirty;

/* Externals, to this file */
extern struct xuiClass ButtonClass;

static GC
create_gc(Display* display)
{
  GC gc;
  XGCValues values;
  int screen_num;

  screen_num = DefaultScreen(display);

  /* allocate foreground and background colors for this GC. */
  values.background = WhitePixel(display, screen_num);
  values.foreground = BlackPixel(display, screen_num);

  gc = XCreateGC(display, DefaultRootWindow(display),
      GCForeground | GCBackground, &values);
  if (gc < 0) {
    fprintf(stderr, "Failed to create GC using XCreateGC.\n");
  }

  /* define the style of lines that will be drawn using this GC. */
  XSetLineAttributes(display, gc,
                     1, /* line width in pixels */
                     LineSolid, /* line style for drawing */
                     CapButt, /* style for the line's edge. */
                     JoinBevel /* style for joined lines. */);

  /* define the fill style for the GC. to be 'solid filling'. */
  XSetFillStyle(display, gc, FillSolid);

  return gc;
}

xuiWnd *
xui_set_focus(xuiWnd *new_focus)
{
  xuiWnd *old_focus = focused_window;

  focused_window = new_focus;
//  XSetInputFocus(xuiAppCtx.display, focused_window->win, RevertToParent, CurrentTime);
  return old_focus;
}

/*
 * Initializes the XUI library, using the provided display name.
 * If display_name is NULL, an attempt is made to get the value
 * from environmental variables.
 */
void
xui_init(const char *display_name)
{
  memset(&xuiAppCtx, 0, sizeof(xuiAppCtx));

  if (display_name != NULL) {
    xuiAppCtx.display = XOpenDisplay(display_name);
  } else {
    char *env_display;

    env_display = getenv("DISPLAY");
    xuiAppCtx.display = XOpenDisplay(env_display);
  }

  /* open connection with the X server. */
  if (xuiAppCtx.display == NULL) {
    fprintf(stderr, "xui: cannot connect to X server '%s'\n",
        display_name);
    exit(1);
  }


  /* Capture the WM_DELETE_WINDOW atom, if it exists. We'll use this later
   * to tell the window manager that we're interested in handling the
   * close/delete events for top level windows */
  WM_DELETE_WINDOW = XInternAtom(xuiAppCtx.display, "WM_DELETE_WINDOW", 0);

  paint_queue = NULL;

  /* Create an X context manager that we can use to map xui Windows to
   * X windows */
  xuiG_window_ctxt = XUniqueContext();

  /* allocate a new GC (graphics context) for drawing in the window. */
  xuiAppCtx.gc = create_gc(xuiAppCtx.display);

  /* Initialize various local variables that we'll use through different
   * functions later on */
  x_fd = ConnectionNumber(xuiAppCtx.display);
  loop_done = false;
  any_win_dirty = false;

  /* Cross module based initialization */

  /* Initialize color */
  xui_res_init();

  /* Register all the window classes */
  xui_class_register(&ButtonClass);
}

xuiWnd *
xui_get_focus(void)
{
  return focused_window;
}

void
xui_cycle_focus(xuiWnd *mainw, int inc)
{
  xuiWnd *old;
  xuiWnd *new;

  old = xui_get_focus();

  new = xui_window_get_sibling(mainw, old, inc < 0);
  xui_set_focus(new);

  xui_window_invalidate(old);
  xui_window_invalidate(new);
}

void
xui_shutdown(void)
{
  XFreeGC(xuiAppCtx.display, xuiAppCtx.gc);

  /* close the connection to the X server. */
  XCloseDisplay(xuiAppCtx.display);
}

static void
handle_keypress_event(XKeyEvent *xkey)
{
  xuiWnd *w;
  KeySym sym;

  XLookupString(xkey, xuiAppCtx.last_event.keytext,
      sizeof(xuiAppCtx.last_event.keytext),
      &sym, NULL);
  xuiAppCtx.last_event.keycode = sym;
  xuiAppCtx.last_event.state = 0;
  if (xkey->state & ShiftMask) {
    xuiAppCtx.last_event.state = KEY_SHIFTMASK;
  }
  /* use the XLookupString routine to convert the invent
     KeyPress data into regular text.  Weird but necessary... */

  w = xui_get_focus();
  if (w->style & WS_TABSTOP) {
    if (xuiAppCtx.last_event.keycode == KEY_Tab) {

      xui_cycle_focus(w->parent, 1);
      return;
    }
    if ((xuiAppCtx.last_event.keycode == KEY_ISO_Left_Tab) ||
        (xuiAppCtx.last_event.keycode == KEY_Tab &&
        (xuiAppCtx.last_event.state & KEY_SHIFTMASK))) {
      xui_cycle_focus(w->parent, -1);
      return;
    }
  }

  xui_window_send(w, MSG_KEYPRESS, 0, &xuiAppCtx.last_event);
}

static void
xui_process_x_event(XEvent *event, xuiWnd *wnd)
{
  int msg;

  switch (event->type) {
  case Expose:
    if (event->xexpose.count == 0) {
      xui_window_invalidate(wnd);
    }
    break;
  case KeyPress:
    handle_keypress_event(&event->xkey);
    break;
  case ButtonPress:
  case ButtonRelease:
    msg = MSG_MOUSEBUTTONDOWN;
    if (event->type == ButtonRelease)
      msg = MSG_MOUSEBUTTONUP;

    xuiAppCtx.last_event.x = event->xbutton.x;
    xuiAppCtx.last_event.y = event->xbutton.y;

    xui_window_send(wnd, msg, 0, &xuiAppCtx.last_event);
    break;
  case EnterNotify:
    XFindContext(event->xany.display, event->xcrossing.subwindow,
        xuiG_window_ctxt, (XPointer *)&wnd);
    xui_window_send(wnd, MSG_MOUSEENTER, 0, NULL);
    break;
  case LeaveNotify:
    xui_window_send(wnd, MSG_MOUSELEAVE, 0, NULL);
    break;
  case ClientMessage:
    if (event->xclient.format == 32 &&
        event->xclient.data.l[0] == WM_DELETE_WINDOW) {
      xui_window_send(wnd, WM_CLOSE, 0, NULL);
    }
    break;
  }
}

static void
xui_process_paints(void)
{
  struct paint_entry *e;

  while (paint_queue != NULL) {
    e = paint_queue;

    xui_window_send(e->wnd, WM_PAINT, 0, NULL);

    if (!e->wnd->is_dirty) {
      paint_queue = e->next;
      free(e);
    }
  }
}

void
xui_window_invalidate(xuiWnd *wnd)
{
  struct paint_entry *entry;

  if (!wnd->is_dirty) {
    wnd->is_dirty = 1;

    /* Add it to paint queue */
    entry = malloc(sizeof(struct paint_entry));
    entry->wnd = wnd;
    entry->next = paint_queue;
    paint_queue = entry;
  }
}

void
xui_loop(void)
{
  XEvent event;
  xuiWnd *wnd;

  /* flush all pending requests to the X server. */
  XFlush(xuiAppCtx.display);

  /* look for events forever... */
  while (1) {

    /* If there's nothing to do see if we can process some Exposes */
    if (!XPending(xuiAppCtx.display)) {
      xui_process_paints();
    }
    XNextEvent(xuiAppCtx.display, &event);

    /* Compress motion events */
    if (event.xany.type == MotionNotify) {
      while (XPending(xuiAppCtx.display)) {
        XEvent e;

        XPeekEvent(xuiAppCtx.display, &e);
        if ((e.xany.type != MotionNotify) ||
            (event.xmotion.window != e.xmotion.window) ||
            (event.xmotion.state != e.xmotion.state))
          break;

        XNextEvent(xuiAppCtx.display, &event);
      }
    }


    XFindContext(event.xany.display, event.xany.window, xuiG_window_ctxt,
        (XPointer *)&wnd);

    /* Not an event for us */
    if (wnd == NULL)
      continue;

    xui_process_x_event(&event, wnd);
  }
}

int
xui_release_selection(xuiWnd *wnd)
{
  if (xuiAppCtx.selection_window == wnd) {
    xuiAppCtx.selection_window = NULL;
    return 1;
  }
  return 0;
}

int
xui_has_selection(xuiWnd *wnd)
{
  return (xuiAppCtx.selection_window == wnd);
}
