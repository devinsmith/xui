/*
 * Copyright (c) 2017 Devin Smith <devin@devinsmith.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <xui.h>
#include "xui_priv.h"

#define XUI_DEFAULT_FONT "7x14"

/* Visual types */
#define VISUALTYPE_UNKNOWN 0       /* Undetermined visual type */
#define VISUALTYPE_MONO    1       /* Visual for drawing into 1-bpp surfaces */
#define VISUALTYPE_TRUE    2       /* True color */
#define VISUALTYPE_INDEX   3       /* Index [palette] color */
#define VISUALTYPE_GRAY    4       /* Gray scale */

struct xui_visual {
  int numred;
  int numgreen;
  int numblue;
  int numcolors;
  int type;
  unsigned long rpix[16][256];          /* Mapping from red -> pixel */
  unsigned long gpix[16][256];          /* Mapping from green -> pixel */
  unsigned long bpix[16][256];          /* Mapping from blue -> pixel */
  Visual *x_visual;
};

static Pixel xui_res_colors[COLOR_MAX];
static struct xuiFont *xui_res_stock_fonts[FONT_MAX];
static int xui_depth;
static struct xui_visual xui_visual;

/* Standard dither kernel */
static const int dither[16] = {
   0*16,  8*16,  2*16, 10*16,
  12*16,  4*16, 14*16,  6*16,
   3*16, 11*16,  1*16,  9*16,
  15*16,  7*16, 13*16,  5*16,
};

static Pixel
makeHilite(Pixel clr)
{
  unsigned int r, g, b;
  r = XUIREDVAL(clr);
  g = XUIGREENVAL(clr);
  b = XUIBLUEVAL(clr);
  r = XUIMAX(31, r);
  g = XUIMAX(31, g);
  b = XUIMAX(31, b);
  r = (133 * r) / 100;
  g = (133 * g) / 100;
  b = (133 * b) / 100;
  r = XUIMIN(255, r);
  g = XUIMIN(255, g);
  b = XUIMIN(255, b);
  return xui_res_get_rgb_color(XUIRGB(r, g, b));
}

static Pixel
makeShadow(Pixel clr)
{
  unsigned int r, g, b;
  r = XUIREDVAL(clr);
  g = XUIGREENVAL(clr);
  b = XUIBLUEVAL(clr);
  r = (66 * r) / 100;
  g = (66 * g) / 100;
  b = (66 * b) / 100;
  return xui_res_get_rgb_color(XUIRGB(r, g, b));
}

/* Find shift amount */
static unsigned int
findshift(unsigned long mask)
{
  unsigned int sh = 0;
  while (!(mask&(1 << sh))) sh++;
  return sh;
}

/* Apply gamma correction to an intensity value in [0..max]. */
static unsigned int
gamma_adjust(double gamma, unsigned int value, unsigned int max)
{
  double x = (double)value / (double)max;
  return (unsigned int) (((double)max * pow(x, 1.0 / gamma)) + 0.5);
}

/* Setup for true color */
static void
setuptruecolor(void)
{
  unsigned int  redshift, greenshift, blueshift;
  unsigned long redmask, greenmask, bluemask;
  unsigned long redmax, greenmax, bluemax;
  unsigned int  i, c, d, r, g, b;
  double gamma;

  /* Ideally we would read from the registry */
  gamma = 1.0;

  /* Arrangement of pixels */
  redmask = xui_visual.x_visual->red_mask;
  greenmask = xui_visual.x_visual->green_mask;
  bluemask = xui_visual.x_visual->blue_mask;
  redshift = findshift(redmask);
  greenshift = findshift(greenmask);
  blueshift = findshift(bluemask);
  redmax = redmask >> redshift;
  greenmax = greenmask >> greenshift;
  bluemax = bluemask >> blueshift;
  xui_visual.numred = redmax + 1;
  xui_visual.numgreen = greenmax + 1;
  xui_visual.numblue = bluemax + 1;
  xui_visual.numcolors = xui_visual.numred * xui_visual.numgreen *
    xui_visual.numblue;

  /* Make the dither tables */
  for (d = 0; d < 16; d++) {
    for(i = 0; i < 256; i++) {
      c = gamma_adjust(gamma, i, 255);
      r = (redmax * c + dither[d]) / 255;
      g = (greenmax * c + dither[d]) / 255;
      b = (bluemax * c + dither[d]) / 255;
      xui_visual.rpix[d][i] = r << redshift;
      xui_visual.gpix[d][i] = g << greenshift;
      xui_visual.bpix[d][i] = b << blueshift;
    }
  }

  /* Set type */
  xui_visual.type = VISUALTYPE_TRUE;
}

static void
setupdirectcolor(void)
{
  printf("direct color TODO\n");
}

static void
setuppseudocolor(void)
{
}

static void
setupstaticcolor(void)
{
}

static void
setupgrayscale(void)
{
}

static void
setupstaticgray(void)
{
}

void
xui_res_init(void)
{
  /* Create copies of X things */
  xuiAppCtx.colormap = DefaultColormap(xuiAppCtx.display,
      DefaultScreen(xuiAppCtx.display));
  xui_visual.x_visual = DefaultVisual(xuiAppCtx.display,
      DefaultScreen(xuiAppCtx.display));
  xui_depth = DefaultDepth(xuiAppCtx.display,
      DefaultScreen(xuiAppCtx.display));

  /* setup color map */
  switch (xui_visual.x_visual->class) {
  case TrueColor:   setuptruecolor(); break;
  case DirectColor: setupdirectcolor(); break;
  case PseudoColor: setuppseudocolor(); break;
  case StaticColor: setupstaticcolor(); break;
  case GrayScale:   setupgrayscale(); break;
  case StaticGray:  setupstaticgray(); break;
  }

  xui_res_colors[COLOR_WHITE] = xui_res_get_rgb_color(XUIRGB(255, 255, 255));
  xui_res_colors[COLOR_BLACK] = xui_res_get_rgb_color(XUIRGB(0, 0, 0));
  /* Older versions of Windows used 192/192/192 (#c0c0c0), but newer
   * versions have a somewhat brighter gray. */
  xui_res_colors[COLOR_FRAMEBACK] = xui_res_get_rgb_color(XUIRGB(0xd4, 0xd0, 0xc8));
  xui_res_colors[COLOR_FRAMEHILITE] = makeHilite(XUIRGB(0xd4, 0xd0, 0xc8));
  xui_res_colors[COLOR_FRAMESHADOW] = makeShadow(XUIRGB(0xd4, 0xd0, 0xc8));


  xui_res_stock_fonts[FONT_NORMAL] = xui_res_create_font(XUI_DEFAULT_FONT);
}

Pixel
xui_res_get_color(int colorIndex)
{
  Pixel ret = 0;
  if (colorIndex < COLOR_MAX && colorIndex >= 0)
    return xui_res_colors[colorIndex];

  return ret;
}

unsigned int
xui_res_get_rgb_color(unsigned int rgbColor)
{
  switch (xui_visual.type) {
  case VISUALTYPE_TRUE:
    return xui_visual.rpix[1][XUIREDVAL(rgbColor)] |
      xui_visual.gpix[1][XUIGREENVAL(rgbColor)] |
      xui_visual.bpix[1][XUIBLUEVAL(rgbColor)];
  case VISUALTYPE_UNKNOWN:
    return 0;
  }

  return 0;
}

struct xuiFont *
xui_res_create_font(const char *name)
{
  struct xuiFont *font;

  font = malloc(sizeof(struct xuiFont));

  font->id = XLoadQueryFont(xuiAppCtx.display, XUI_DEFAULT_FONT);

  return font;
}

const struct xuiFont *
xui_res_get_stock_font(int fntIndex)
{
  struct xuiFont *f = NULL;
  if (fntIndex < FONT_MAX && fntIndex >= 0)
    return xui_res_stock_fonts[fntIndex];

  return f;
}
